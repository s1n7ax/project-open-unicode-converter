import RuleModel from "../../models/rule-model";
import KeyBindDetailsModel from "../../models/key-bind-details-model";
import IConverter from "../../i-converter";
import ILanguageModifier from "../../i-language-modifier";
import ConfigModel from "../../models/config-model";

/**
 * Sinhala Unicode convert
 */
export default class SinhalaConverter implements IConverter {
    modifiers: Array<ILanguageModifier>;
    config: ConfigModel;
    id: string = 'sinhala-singlish';
    name: string = 'Sinhala (singlish)';


    /**
     * @todo
     *
     * modifiers should be called before and after
     */
    public convert(key: string, currentWord: string,): RuleModel {
        let keyBindDetails: KeyBindDetailsModel = this.config.mapping.get(key);

        // return same character if no match found in key binding details
        if(!keyBindDetails)
            return new RuleModel(null, key, 0);

        // return default char if no additional rules are found
        if(!keyBindDetails.rules)
            return new RuleModel(null, keyBindDetails.char, 0);

        // handle rules
        let ruleResult = this.handleRules(currentWord, keyBindDetails);
        if(ruleResult)
            return ruleResult;

        // if match found but not matched to any rules, default character will be returned
        return new RuleModel(null, keyBindDetails.char, 0);
    }

    addModifier(modifier: ILanguageModifier): void {
        this.modifiers.push(modifier);
    }

    removeModifier(modifier: ILanguageModifier): void {
        this.modifiers = this.modifiers.filter(mod => mod !== modifier);
    }


    // helper functions
    private handleRules(currentWord: string, keyBindDetails: KeyBindDetailsModel) {
        // looping through rules to find a match
        for(let rule of keyBindDetails.rules) {

            // check defined variables
            let variableRegex = new RegExp(/<%([\w\d]*)%>/);
            let match = variableRegex.exec(rule.match);

            if(match !== null) {
                let variableValArr: Array<string> = this.config.variables.get(match[1]);

                // throw if the used variable is not defined in the variable section in the configuration
                if(!variableValArr)
                    throw new Error("No configuration variable found with key: " + match[1]);

                // get the last letter from the current word and match with variable value array for match
                let matchIndex = variableValArr.indexOf(currentWord[currentWord.length - 1]);
                if(~matchIndex)
                    return rule;
            }


            // if the length of the currentWord is higher than the rule iteration will be skipped
            if(currentWord.length < rule.match.length)
                continue;

            let currentWSubstring = currentWord.substring(
                currentWord.length - rule.match.length,
                currentWord.length
            );

            // if a rule matched the history rule will be returned
            if(rule.match === currentWSubstring)
                return rule;
        }
    }
}