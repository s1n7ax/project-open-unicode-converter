import IConverter from "../../i-converter";
import SinhalaConfigProvider from "./sinhala-config-provider";
import SinhalaConverter from "./sinhala-converter";
import IConverterManager from "../../i-converter-manager";
import StaticImplements from "../../common/static-implements";

@StaticImplements<IConverterManager>()
export default class SinhalaConverterManager {

    static async getInstance(): Promise<IConverter> {
        // config fetch url
        let host = document.location.href;
        let url = `${host}/unicode-converters/sinhala/config/sinhala-uni-config.json`;

        // get the config through the adaptor
        let config = await new SinhalaConfigProvider().getConfig(url);

        // get the converter
        let converter = new SinhalaConverter();
        converter.config = config;

        return converter;
    }
}