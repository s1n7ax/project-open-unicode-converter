import IConverterFactory from "../i-converter-factory";
import IConverter from "../i-converter";
import SinhalaConverterManager from "./sinhala/sinhala-converter-manager";

export default class ConverterFactory implements IConverterFactory{
    private converters: Map<string, IConverter> = new Map();

    get(name: string): IConverter {
        if(!this.converters)
            throw new Error("Converter Factory is not initialized ");

        return this.converters.get(name);
    }

    getConverters(): Array<string> {
        return Array.from(this.converters.keys());
    }

    static async initialize(): Promise<ConverterFactory> {
        let cf = new ConverterFactory();

        // converters
        let sinhala = await SinhalaConverterManager.getInstance();

        cf.converters.set(sinhala.id, sinhala);

        return cf;
    }
}
