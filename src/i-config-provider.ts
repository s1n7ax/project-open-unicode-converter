import ConfigModel from "./models/config-model";

/**
 * Config provider implements the process of getting configuration from any source  and
 * converting it to a data source that unicode converter understands
 */
export default interface IConfigProvider {
    /**
     * Get the data from any available source and returning converter readable data source
     *
     * @param data
     *  any data that is required by the config provider
     *
     * @return
     *  returns the configuration map
     */
    getConfig(...data: Array<any>): Promise<ConfigModel>;
}