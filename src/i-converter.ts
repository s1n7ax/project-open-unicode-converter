import RuleModel from "./models/rule-model";
import ILanguageModifier from "./i-language-modifier";
import ConfigModel from "./models/config-model";

/**
 * Convert letters from language to another
 *
 * @property modifiers
 *  holds list of functions that is subscribe to result from convert() method which change or modify and return final value
 *  NOTE: converter implementation defines the way modifiers are executed
 *
 * @property config
 *  holds configuration details
 *
 * @property id
 *  unique id for converter class type
 *  id will be used to identify the object in the program
 *
 * @property name
 *  display name for the object
 *  name can be used to display user friendly name in the UI
 */
export default interface IConverter {
    modifiers: Array<ILanguageModifier>;
    config: ConfigModel;
    id: string;
    name: string;

    /**
     * Return language conversion details from one to another language
     *
     * @param letter
     *  the letter that should be returned conversions details for
     *
     * @param currentWord
     *  language conversion may depend on the letters present before
     *  in a scenario like that history can be passed to predict the conversion
     */
    convert(letter: string, currentWord?: string): RuleModel;

    /**
     * Adds modifier function to modifier list
     *
     * @param modifier
     *  modifier object
     */
    addModifier(modifier: ILanguageModifier): void;

    /**
     * Remove modifier from the modifier list
     *
     * @param modifier
     *  modifier object
     */
    removeModifier(modifier: ILanguageModifier): void;
}