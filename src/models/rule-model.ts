export default class RuleModel {
    match: string;
    result: string;
    remove: number;

    constructor(match: string, result: string, remove: number) {
        this.match = match;
        this.result = result;
        this.remove = remove;
    }
}