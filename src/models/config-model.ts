import KeyBindDetailsModel from "./key-bind-details-model";

export default class ConfigModel {
    variables: Map<string, Array<string>>;
    mapping: Map<string, KeyBindDetailsModel>;


    constructor(variables: Map<string, Array<string>>, mapping: Map<string, KeyBindDetailsModel>) {
        this.variables = variables;
        this.mapping = mapping;
    }
}