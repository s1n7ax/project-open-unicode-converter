import IConverter from "./i-converter";

/**
 * Initialize converter object for a language
 */
export default interface IConverterManager {

    /**
     * Initialize and returns a converter object
     */
    getInstance(): Promise<IConverter>;
}