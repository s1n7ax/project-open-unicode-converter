import IConverter from "./i-converter";

export default interface IConverterFactory {

    /**
     * get converter by name
     * @param name name of the converter
     */
    get(name: string): IConverter;

    /**
     * return list of available converters
     */
    getConverters(): Array<string>;
}